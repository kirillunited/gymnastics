var gulp = require("gulp");
var pagebuilder = require('gulp-file-include');
const pug = require('gulp-pug');

gulp.task('pug', function () {
    return gulp.src(['src/pug/**/*.pug', '!./node_modules/**'])
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('src/html/views'));
});

var path = {
    src: {
        html: 'src/html/*.html'
    },
    build: {
        html: 'build/'
    }
}
var sftp = require('gulp-sftp');
gulp.task( 'deploy', function () {
    return gulp.src('build/**/*')
        .pipe(sftp({
            host: 'html.dev-bitrix.by',
            user: 'Shtml',
            pass: 'eig3SaiZ',
            remotePath: "/home/hdd/html/htdocs/Dubov_K/gymnastic-federation/"
        }));
});

gulp.task('inject', function () {
    var htmlInject = gulp.src(path.src.html)
        .pipe(pagebuilder({
            prefix: '@@',
            basepath: '@file',
            context: {
                //Accreditation
                accredit: {
                    Options: ['XLS', 'PDF', 'All Photos', 'All Passports'],
                    Cols: ['COUNTRY', 'FULL NAME', 'FUNCTION', 'PHOTO', 'PASSPORT', 'VISA', ]
                },
                //Music
                music: {
                    Options: ['XLS', 'PDF', 'All Music', 'All Passports'],
                    Cols: ['Antem', 'RGI', 'Country/Login', ]
                },
                // Transport
                transport: {
                    Options: ['XLS', 'PDF', 'All Photos', 'All Passports'],
                    Cols: ['Arrivals', 'Departures', 'FUNCTION', 'PHOTO', 'PASSPORT', 'VISA', ]
                },
                // Accommodation
                accom: {
                    Options: ['XLS', 'PDF', 'All Photos', 'All Passports'],
                    Cols: ['Login/Country', 'Hotel', 'Room', 'Price per Room', 'Check in', 'Check out', 'Check out', 'Nights', 'Number of Rooms', 'Total']
                },
                // Meals
                meals: {
                    Options: ['XLS', 'PDF', 'All Photos', 'All Passports'],
                    Cols: ['Login/Country', 'Meal Type', 'Price', 'Date from', 'Date to', 'Days', 'Quantity', 'Total' ]
                },
                // Invoice
                invoice: {
                    Options: ['XLS', 'PDF', 'All Photos', 'All Passports'],
                    Cols: ['Login/Country', 'Expenses', 'Total',]
                },
            }
        }))
        .pipe(gulp.dest(path.build.html));
});

gulp.task('watch', ['inject', 'pug'], function () {
    gulp.watch('src/**/*', ['inject']);
    gulp.watch('src/**/*', ['pug']);
});

gulp.task('default', ['watch']);