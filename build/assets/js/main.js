$(document).ready(function () {
    //reg tabs
    var tabsLink = $('.registration_tab');
    var tab = $('.registration_table');
    $(tabsLink).each(function (i) {
        $(this).attr('data-target', i);
    });
    $(tab).each(function (i) {
        $(this).attr('data-tab', i);
    });
    $(tabsLink).on('click', function () {
        var dataTab = $(this).attr('data-target');
        var tabs = $('.registration_tables');
        $(this).siblings(tabsLink).removeClass('active');
        $(this).addClass('active');
        tabs.find('[data-tab="' + dataTab + '"]').siblings(tabs).removeClass('active');
        tabs.find('[data-tab="' + dataTab + '"]').addClass('active');
    });
    //dropdown menu select
    $('body').on('click', '.form-select_toggle', function (event) {
        $(this).next('.form-select_menu').toggleClass("hidden")
        $(this).parents('.form-group')
            .siblings()
            .find('.form-select_menu:visible')
            .addClass("hidden");
        return false;
    });
    $(document).click(function (e) {
        var el = $('.form-select');
        if (!$(el).is(e.target) && $(el).has(e.target).length === 0) {
            $(el).find('.form-select_menu').addClass('hidden');
        };
    });
    $('body').on('click', '.form-select_item', function () {
        var drop = $(this).parents('.form-select');
        var menu = drop.find('.form-select_menu');
        var inp = menu.find('input[type=radio]');
        var valueSend = $(this).closest('.form-group').find('[data-required]');
        var arr = [];
        $(inp).each(function () {
            if ($(this).prop('checked') == true) {
                arr.push($(this).val());
            }
        });
        if (arr.length > 0) {
            drop.find('.name').text(arr);
        } else if (arr.length == 0) {
            drop.find('.value').text('');
        }
        $(valueSend).val($(this).find('input').val());
        // console.log(valueSend.val());

        menu.addClass('hidden');
    });
    // preview images
    $('body').on('change', '#photo, #passport', function () {
        var preview = $(this).parent().find('.img-preview img');
        var btnUpload = $(this).parent().find('.upload');
        var fileImg = 'assets/img/files.png';
        var file = this.files;
        var reader = new FileReader();


        // размер загружаемого изображения
        var props = {
            width: 480,
            height: 640
        }
        //scale
        var minX = 0.5,
            maxX = 2,
            midX = (minX + maxX) / 2;

        if (file[0] !== undefined) {
            $(preview.parent()).find('.error').remove();

            reader.readAsDataURL(file[0]);
            console.log(file[0], btnUpload);

            if ($(this).is('#photo')) {
                reader.onload = function (e) {
                    var src = e.target.result;

                    var img = document.createElement('img');
                    img.src = reader.result;

                    $(img).ready(function () {
                        $(btnUpload.parent()).find('.error_label.error_label--format').remove();
                        $(btnUpload).parent().removeClass('error error--format').find('.error__format').text('');

                        if (file[0].type === 'image/jpeg' || file[0].type === 'image/png') {
                            if (img.naturalWidth < props.width || img.naturalHeight < props.height) {
                                let errorText = `Image size must be at least ${props.width}x${props.height} pixels`;

                                $(btnUpload).parent().addClass('error').find('.error__format').text(errorText);
                                $(preview).parent().siblings('[type=file]').val('');
                            } else {
                                $(btnUpload).parent().removeClass('error').find('.error__format').text('');
                                $('.image-editer__img').attr('src', src);
                                $('#modalImgCrop').fadeIn();
                                cropIt();
                                zoomRange();
                                // uploadAsImage(file, src, preview, btnUpload, fileImg);
                            }
                        } else {
                            $(btnUpload.parent()).addClass('error error--format');
                            $(btnUpload.parent()).append('<span class="error_label error_label--format">Invalid format!</span>');
                            $(preview).parent().siblings('[type=file]').val('');
                        }
                    });

                };
            }
            if ($(this).is('#passport')) {
                reader.onload = function (e) {
                    var src = fileImg;
                    uploadAsImage(file, src, preview, btnUpload, fileImg);
                };
            }
        }
    });
    //del preview
    $('body').on('click', '.img-preview .del, .media .del', function () {
        var preview = $(this).parent().find('img');
        var btnUpload = $(this).parent().siblings('.upload');
        var inp = $(this).parent().siblings('[type=file]');

        if ($(this).is('.media .del')) {
            preview = $(this).parent().find('audio');
        }
        deleteUpload(btnUpload, preview, inp);
        return false;
    });
    //del media
    // $('body').on('click', '.media .del', function (e) {
    //     e.preventDefault();
    //     // preview = $(this).parent().find('audio');
    //     $(this).parents('.media').addClass('in-proccess');
    //     actionConfirm(null);
    // });
    //remove item
    // $('body').on('click', '[data-attr=removeItem]', function (e) {
    //     e.preventDefault();
    //     var target = $('.media.in-process');
    //     var preview = $(target).find('audio');
    //     var btnUpload = $(target).siblings('.upload');
    //     var inp = $(target).siblings('[type=file]');
    //     console.log(target);

    //     $(btnUpload).removeClass('hidden');
    //     $(preview.parent()).addClass('hidden');
    //     $(preview).attr('src', '');
    //     $(inp).val('');
    //     $(this).parents('.popup').fadeOut();
    //     $('.overlay').fadeOut();
    // });
    //validate form
    $("form").submit(function () {
        var inp = $(this).find('[data-required]');
        $(this).find(inp).removeClass('error');
        var size = 0;
        $(inp).each(function () {
            if ($(this).is('[type=checkbox]')) {
                if ($(this).prop('checked') == true) {
                    $(this).parent().find('label:first').removeClass('error');
                } else {
                    $(this).parent().find('label:first').addClass('error');
                    size += 1;
                }
            }
            if (!($(this).val() === '')) {
                $(this).parent().removeClass('error');
            } else {
                $(this).parent().addClass('error');
                size += 1;
            }
        });
        // input:radio validate
        var inpRadio = $(this).find('[type=radio][data-required]');
        if (inpRadio.is(':checked')) {
            $(inpRadio).parent().find('label:first').removeClass('error');
        } else {
            $(inpRadio).parent().find('label:first').addClass('error');
            size += 1;
        }
        if (size > 0) {
            scrollToErrorInput($(this));
            return false;
        };
        $.post(
            $(this).attr('action'),
            $(this).serialize()
        );
        inp.val('');
        // var preview = $(this).parent().find('img');
        // var btnUpload = $(this).parent().siblings('.upload');
        // var inpFile = $(this).parent().siblings('[type=file]');
        // deleteUpload(btnUpload, preview, inpFile);
        closePopup($(this));
        $('.overlay, .content_overlay').fadeOut();
        return false;
    });
    //user popup
    $('body').on('click', '[data-target="modal"]', function () {
        var id = $(this).attr('href');
        $(id).fadeIn();
        $('.content_overlay').fadeIn();
        return false;
    });
    //upload audio
    $('body').on('change', '[accept=".mp3, .wav"]', function () {
        console.log('connect');
        var file = this.files,
            audio = $(this).parent().find('audio');
        var preview = $(this).parent().find('.media');
        var btnUpload = $(this).parent().find('.upload');

        $(preview.parent()).find('.error').remove();
        if (file !== undefined) {
            audio.attr('src', URL.createObjectURL(file[0]));
            uploadAsAudio(file, preview, btnUpload);
        }
    });
    // $('body').on('click', '.player', function () {
    //     $(this).parent().find('.player').toggleClass('hidden');
    //     return false;
    // });
    //audio
    $('audio').on('playing', function (event) {
        $(this).siblings('.player').toggleClass('hidden');
        console.log(event);
    });
    $('audio').on('pause', function (event) {
        $(this).siblings('.player').toggleClass('hidden');
        console.log(event);
    });
    var audio = document.getElementsByTagName("audio");
    $('body').on('click', '.registration_tab', function () {
        if (!$('.registration_table.music').hasClass('active')) {
            for (let i = 0; i < audio.length; i++) {
                audio[i].pause();
            }
            console.log('pause');
        }
    })
    //observing date change
    $('body').on('change', '[data-attr=date]', function () {
        var val = $(this).val();
        alert($(this).siblings('label').text() + ": " + val);
    });
    //pagination 
    $('body').on('click', '.navigation a', function () {
        if ($(this).is('.pagination .list a')) {
            $(this).closest('li').siblings().removeClass('active');
            $(this).closest('li').addClass('active');
        }
        return false;
    });
    //sort 
    $('body').on('click', '[data-attr="sort"]', function () {
        $(this).parent().siblings().find('[data-attr="sort"]').removeClass('active');
        $(this).addClass('active');
        return false;
    });
    //media modal
    $('body').on('click', '[data-attr=view-img]', function () {
        var src = $(this).attr('href');
        $('.media-modal_img').attr('src', src);
        $('.media-modal').fadeIn();
        return false;
    });
    $('body').on('click', '.media-modal', function (e) {
        if ($(this).is(e.target) && $(this).has(e.target).length === 0 || $(this).find('.close').is(e.target)) {
            $(this).fadeOut();
        };
        return false;
    });
    //sort-date
    $('.sort-date').hover(function () {
        $(this).parent().find('.sort-date').addClass('hover');
    }, function () {
        $(this).parent().find('.sort-date').removeClass('hover');
    });
    //remove error on input
    $('body').on('input change keyup blur', '[data-required]', function () {
        $(this).closest('.error').removeClass('error');
        if ($(this).is(':radio')) {
            $(this).parent().find('.error').removeClass('error');
        }
    });
});
//play audio
function playAudio(el, event) {
    event.preventDefault();
    $.each($('audio'), function (indexInArray, valueOfElement) {
        $(valueOfElement).get(0).pause();
    });
    el.siblings('audio').get(0).play();
}
//  //зацикливание аудио
//  var audio = document.getElementsByTagName("audio");
//  for (let i = 0; i < audio.length; i++) {
//      // audio[i].addEventListener('ended', function () {
//      //     audio[i].play();
//      // }, false);
//      audio[i].addEventListener('pause', function () {
//          $(audio[i]).siblings('.player--stop').removeClass('hidden');                        
//          $(audio[i]).siblings('.player--play').addClass('hidden');    
//          console.log('stop');

//      }, false);
//  }
//btn upload files
function onInputClick(el, e) {
    e.preventDefault();
    $(el).prev('input').click();
}
//uploadAsImg
function uploadAsImage(file, src, preview, btnUpload, fileImg) {
    if (file[0].type === 'image/jpeg' || file[0].type === 'image/png') {
        $(btnUpload).addClass('hidden');
        $(preview.parent()).removeClass('hidden');
        $(preview).attr('src', src);
        $(preview).removeClass('hidden');
        $(btnUpload.parent()).removeClass('error error--format');
        $(btnUpload.parent()).find('.error_label--format').remove();
    } else if (file[0].type === 'application/pdf') {
        $(btnUpload).addClass('hidden');
        $(preview.parent()).removeClass('hidden');
        $(preview).attr('src', fileImg);
        $(preview).removeClass('hidden');
        $(btnUpload.parent()).removeClass('error error--format');
        $(btnUpload.parent()).find('.error_label--format').remove();
    } else {
        // $(btnUpload).addClass('hidden');
        // $(preview.parent()).removeClass('hidden');
        // $(preview).before('<div class="error error--format">Invalid format!</div>');
        // $(preview).attr('src', '');
        // $(preview).addClass('hidden');
        $(btnUpload.parent()).addClass('error error--format');
        $(btnUpload.parent()).append('<span class="error_label error_label--format">Invalid format!</span>');
        $(preview).parent().siblings('[type=file]').val('');
    }
}
//up;oad audio
function uploadAsAudio(file, preview, btnUpload) {
    if (file[0].type === 'audio/mp3' || file[0].type === 'audio/wav') {
        $(btnUpload).addClass('hidden');
        $(preview).removeClass('hidden');
    } else {
        $(preview).before('<div class="error">Invalid format!</div>');
        $(preview).find('audio').attr('src', '');
        $(preview).parent().find('[type=file]').val('');
    }
}
//click player
function playerFunc(el, e) {
    e.preventDefault();
    $(el).siblings('audio').click();
}
//del upload
function deleteUpload(btnUpload, preview, inp) {
    $(btnUpload).removeClass('hidden');
    $(preview.parent()).addClass('hidden');
    $(preview).attr('src', '');
    $(inp).val('');
}
//close popup 
function closePopup(el) {
    $(el).parents('.popup').fadeOut();
    $('.overlay, .content_overlay').fadeOut();
    $('.media').removeClass('in-process');
}
//confirm action 
function actionConfirm(el) {
    var modal = $('#popupConfirm');
    var overlay = $('.overlay');

    // $(el).parents('.popup').fadeOut();
    $(modal).fadeIn().addClass('open');
    $(overlay).fadeIn();
}
//scroll to error input
function scrollToErrorInput(form) {
    var inp = $(form).find('.error');
    var top = $(inp).offset().top;
    $('html, body').stop().animate({
        scrollTop: top - 20
    }, 1000);
}

/**
 * JS
 * ТЗ этап 2
 */
$(function () {
    /**
     * @param categoryArr - массив с выбранными Functions
     * @func guestsCheck() - проверка выбранным гостей
     */
    var categoryArr = [];
    var categoryIsChanged = false;

    $('body').on('click', '.drop-select__item', function () {
        var drop = $(this).parents('.drop-select');
        var menu = drop.find('.drop-select__menu');
        var inp = menu.find('input');
        var arr = [];
        $(inp).each(function () {
            if ($(this).prop('checked') == true) {
                arr.push($(this).val());
            }
        });
        categoryArr = arr;
        categoryIsChanged = true;
    });

    $(document).click(function (e) {
        if (!$('.drop-select__menu, .drop-select__btn').is(e.target) && $('.drop-select__menu').has(e.target).length === 0) {
            $('.drop-select__menu').hide();
            if (categoryIsChanged) filterFunctions();
            categoryIsChanged = false;
        };
    });

    $('body').on('click', '.drop-select__btn', function (e) {
        e.stopPropagation();
        $(this).parent().find('.drop-select__menu').toggle();
        if (categoryIsChanged) filterFunctions();
        categoryIsChanged = false;
    });

    /**button delete all functions in add guests popup */
    const RESET_BTN = '<div class="val-reset">\
                        <span class="val-reset__text">reset filters</span>\
                    </div>';

    function filterFunctions() {
        var drop = $(".drop-select");
        var defaultText = 'Function';

        if (categoryArr.length > 0) {
            drop.find('.drop-select__btn .val').text(categoryArr).addClass('is_selected');
            if (drop.find('.val-reset').length === 0) {
                drop.find('.drop-select__btn').after(RESET_BTN);
            }
        } else {
            drop.find('.drop-select__btn .val').text(defaultText).removeClass('is_selected');
            drop.find('.val-reset').remove();
        }

        var clearFilter = document.querySelector('.val-reset');

        if (clearFilter) {
            clearFilter.addEventListener('click', function () {
                let checkbox = $(clearFilter).parents('.drop-select').find('input');

                $.each(checkbox, function () {
                    $(this).prop('checked', false);
                });

                categoryArr = [];
                filterFunctions();
            }, true);
        }
    }

    $('body').on('keyup change blur click', '.guests input', function () {
        var form = $(this.form);
        guestsCheck(form);
    });

    /**сабмит формы добавления гостей */
    $('body').on('submit', '#ADD_GUESTS_FORM', function (event) {
        event.preventDefault();
        var form = $(this);
        var inp = form.find(".guests__label:not(.is_disabled) input[type=checkbox]");
        var container = $('[name=ADD_GUESTS]');
        var inpValArr = [];

        var inpIsChecked = inp.filter(function () {
            return $(this).prop("checked") === true;
        });

        $.each(inpIsChecked, function () {
            var value = $(this).val();

            inpValArr.push(value);
        });

        container.val(inpValArr.join(', '))
            .siblings('.add')
            .text(container.val())
            .addClass('is_edited');

        $(this).parents(".popup").fadeOut();
    });
});

function guestsCheck(form) {
    var numOfGuests = 2; /* - доступное для выбора кол-во гостей*/
    var inp = form.find(".guests__label:not(.is_disabled) input[type=checkbox]");
    var inpValArr = [];

    var inpIsChecked = inp.filter(function () {
        return $(this).prop("checked") === true;
    });

    var inpUnChecked = inp.filter(function () {
        return $(this).prop("checked") === false;
    });

    $.each(inpIsChecked, function () {
        var value = $(this).val();

        inpValArr.push(value);
    });

    if (inpIsChecked.length == numOfGuests) {
        form.find("[type=submit]").prop("disabled", false);
        inpUnChecked.siblings('.form_check').hide();
        inpUnChecked.parent().addClass('disabled');
        return true;
    }

    form.find("[type=submit]").prop("disabled", true);
    inpUnChecked.siblings('.form_check').show();
    inpUnChecked.parent().removeClass('disabled');
}

/**image crop ТЗ этап 2*/
$(function () {
    $('body').on('submit', '#ADD_CROPED_IMG', function (event) {
        event.preventDefault();

        // размер загружаемого изображения
        var props = {
            width: 480,
            height: 640,
        }

        $('.image-editer__img').cropper("getCroppedCanvas", {
            width: props.width,
            height: props.height,
            maxWidth: 4096,
            maxHeight: 4096,
            fillColor: '#fff',
        }).toBlob(function (blob) {
            var imgUpload = $("#photo");
            var form = imgUpload.form();
            var preview = imgUpload.parent().find('.img-preview img');
            var btnUpload = imgUpload.parent().find('.upload');

            var file = blob;
            var reader = new FileReader();

            // const formData = new FormData(form);

            // formData.append('croppedImage', blob);

            reader.readAsDataURL(file);

            reader.onload = function (e) {
                var src = reader.result;

                $(btnUpload).addClass('hidden');
                $(preview.parent()).removeClass('hidden');
                $(preview).attr('src', src);
                $(preview).removeClass('hidden');
                $(btnUpload.parent()).removeClass('error error--format');
                $(btnUpload.parent()).find('.error_label--format').remove();
            };
        }, 'image/jpeg');

        $(this).parents(".popup").hide();
        $(".image-editer__img").cropper("destroy");

    });

    /**vertical zoom range */
    //scale
    var minX = 0.5,
        maxX = 2,
        midX = (minX + maxX) / 2;

    zoomRange();

    /**scroll list on click*/
    $('body').on('click', '.scroll-top, .scroll-bottom', function (e) {
        e.preventDefault();
        var box = $(this).siblings('.form-select_menu-wrap');

        box.animate({
            scrollTop: $(this).is('.scroll-top') ? '-=55' : '+=55'
        });
    });
});

/**crop img init */
function cropIt() {
    var $image = $('.image-editer__img');

    $image.cropper({
        aspectRatio: 3 / 4,
        dragMode: "move",
        background: false,
        zoomOnWheel: false,
        cropBoxMovable: false,
        cropBoxResizable: false,
        viewMode: 1,
    });

    $image.on('ready', function () {
        $(this).cropper("scale", 0.8);
    });

    // Get the Cropper.js instance after initialized
    var cropper = $image.data('cropper');
}

/**vertical zoom range init */
function zoomRange() {

    $(".image-cropper .zoom__range").slider({
        orientation: "vertical",
        range: "max",
        min: 8,
        max: 100,
        value: 8,
        slide: function (event, ui) {
            $(".image-editer__img").cropper("scale", ui.value / 10);
        }
    });
}